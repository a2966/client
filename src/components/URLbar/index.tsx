import React, { useContext, useState } from "react";
import { useEffect } from "react";
import { apiMethods } from "../../constants";
import { MainContext } from "../../context";
import { SubplaygroundProps } from "../../types";

const URLBar: React.FC<SubplaygroundProps & { callApi: () => void }> = ({
  requestConfig,
  callApi,
  setRequestConfig,
}) => {
  const { contextData } = useContext(MainContext);
  const { base_url } = contextData.data;

  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState(() =>
    contextData.selectedIndex === -1 ? "" : base_url + requestConfig.url
  );

  useEffect(() => {
    setSearch(
      contextData.selectedIndex === -1 ? "" : base_url + requestConfig.url
    );
  }, [requestConfig.url, contextData.selectedIndex, base_url]);

  const handleSend = async () => {
    // function to handle send
    console.log("Send btn clicked");
    setLoading(true);

    await callApi();

    setLoading(false);
  };

  const handleSave = () => {
    // function to handle send
    console.log("Save btn clicked");
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.indexOf(base_url!) === 0) {
      let url = e.target.value.slice(base_url.length);
      setSearch(e.target.value);
      setRequestConfig((prev) => {
        return { ...prev, url };
      });
    }
  };

  return (
    <div className='w-full h-16 flex justify-around items-center border-b-2 pt-0 mt-0'>
      <div className='w-1/12 h-2/3'>
        <select
          value={requestConfig.method}
          className='rounded outline-none h-full w-full bg-gray-100 text-sm px-2'
        >
          {apiMethods.map((item, idx) => (
            <option key={item.value} className='font-medium'>
              {item.value}
            </option>
          ))}
        </select>
      </div>
      <div className='w-4/5 h-2/3 pl-2'>
        <input
          value={search}
          onChange={(e) => handleChange(e)}
          className='rounded outline-none h-full w-full p-4 border border-gray-200 text-gray-500 bg-gray-100'
        />
      </div>
      <div className='w-1/12  h-2/3 flex justify-center'>
        <button
          className='rounded h-full text-center w-4/5 bg-blue-500 text-light2'
          onClick={handleSend}
        >
          Send
        </button>
      </div>
      <div className='w-1/12  h-2/3 flex justify-center'>
        <button
          className='rounded h-full w-4/5 bg-gray-200'
          onClick={handleSave}
        >
          Save
        </button>
      </div>
    </div>
  );
};

export default URLBar;
