import React, { useContext, useEffect, useState } from 'react';
import { MainContext } from '../../context';
import { Table } from '../../reusables';
import { Alert } from '../../reusables/components/alert';
import { UpperTabs } from '../../reusables/components/upperTabs';
import { async_func_data } from '../../reusables/functions/axios';
import { RequestConfig } from '../../types';
import Editor from '../Editor';
import URLBar from '../URLbar';

const Playground: React.FC = () => {
  const [currentTab, setCurrentTab] = useState<number>(1);

  const { contextData } = useContext(MainContext);
  const { base_url } = contextData.data;

  const [requestConfig, setRequestConfig] = useState<RequestConfig>({
    headers: [{ key: 'Content-Type', value: 'application/json' }],
    query_params: [],
    body: {},
    method: 'GET',
    url: '',
    id: -1,
    name: '',
  });

  useEffect(() => {
    let obj = contextData.data.api_details?.filter(
      (item) => item.id === contextData.selectedIndex
    )[0]!;
    if (obj && Object.keys(obj).length) setRequestConfig(obj);
  }, [contextData.selectedIndex, contextData.data.api_details]);

  console.log(requestConfig);

  const callApi = async () => {
    const params = {};
    Object.entries(requestConfig.query_params).forEach(([key, val]) => {
      params[key] = val;
    });
    const headers = {};
    Object.entries(requestConfig.headers).forEach(([key, val]) => {
      headers[key] = val;
    });
    try {
      const resp = await async_func_data(
        base_url + requestConfig.url,
        requestConfig.method,
        params,
        headers,
        requestConfig.body
      );
      console.log(resp);
    } catch (error) {
      console.log(error);
    }
  };

  const display = () => {
    switch (currentTab) {
      case 1:
        return (
          <Table
            requestConfig={requestConfig}
            setRequestConfig={setRequestConfig}
            currentTab={currentTab}
            currentKey='query_params'
          />
        );
      case 2:
        return (
          <Table
            requestConfig={requestConfig}
            setRequestConfig={setRequestConfig}
            currentTab={currentTab}
            currentKey='headers'
          />
        );
      case 3:
        return (
          <Editor
            requestConfig={requestConfig}
            setRequestConfig={setRequestConfig}
          />
        );
    }
  };

  return (
    <div className='h-full w-full p-4 overflow-hidden relative z-0'>
      <URLBar
        requestConfig={requestConfig}
        setRequestConfig={setRequestConfig}
        callApi={callApi}
      />
      <UpperTabs currentTab={currentTab} setCurrentTab={setCurrentTab} />
      {display()}
      <Alert />
    </div>
  );
};

export default Playground;
