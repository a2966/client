import React, { useContext } from "react";
import { MainContext } from "../../context";
import Tags from "../../reusables/components/tags";
import { ApiListsProps } from "../../types/searchBarTypes";

const ApiLists: React.FC<ApiListsProps> = ({ list }) => {
  const { setContextData, contextData } = useContext(MainContext);

  const handleClick = (id: number) => {
    setContextData((prev) => {
      return {
        ...prev,
        selectedIndex: id,
      };
    });
  };

  if (list.length === 0) {
    return (
      <>
        <img
          src="/images/nodata.svg"
          alt="No data found"
          className="h-40 w-40"
        />
        <p className="mt-7 text-dark1 font-medium">No data found</p>
      </>
    );
  }

  return (
    <div className="flex flex-1 flex-col w-full">
      {list.map((item, _) => {
        return (
          <div
            key={item.id}
            className={`flex flex-col p-4 border-b-2 border-gray-50 hover:bg-gray-100 cursor-pointer ${
              item.id === contextData.selectedIndex && "bg-gray-100"
            }`}
            onClick={() => handleClick(item.id)}
          >
            <div className="flex justify-between mt-2 mb-1">
              <p className="font-medium text-dark2">{item.name}</p>
              <Tags method={item.method} />
            </div>
            <small className="text-xs text-gray-300">{item.url}</small>
          </div>
        );
      })}
    </div>
  );
};

export default ApiLists;
