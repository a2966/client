import React, { useContext, useState } from "react";
import { MainContext } from "../../context";
import { useDebounce } from "../../hooks";
import SearchBar, {
  UseSearchComponent,
} from "../../reusables/components/searchbar";
import { List } from "../../types/searchBarTypes";
import ApiLists from "./ApiLists";

const SideTab: React.FC = () => {
  const { contextData } = useContext(MainContext);

  const { searchProps } = UseSearchComponent("");
  const { value } = searchProps;

  const [list, setList] = useState(contextData.data.api_details);

  const cb = (value: string) => {
    setList(() => {
      let a = contextData.data.api_details?.slice();
      let b = a?.filter((item) =>
        item.name.toLowerCase().includes(value.toLowerCase())
      );
      return b;
    });
  };

  useDebounce(cb, 500, value);

  return (
    <div className="h-full w-full pt-4 pb-4 flex flex-col items-center border-r-2 border-gray-100">
      <div className="flex flex-col items-center w-full">
        {/* Searchbar */}
        <SearchBar {...searchProps} />
        {/* ApiList */}
        <ApiLists list={list as List[]} />
      </div>
    </div>
  );
};

export default SideTab;
