import React, { useEffect, useState } from "react";
import { Controlled as CodeEditor } from "react-codemirror2";
import { SubplaygroundProps } from "../../types";

// for bracket auto-complete
import "codemirror/addon/edit/closebrackets";
import "codemirror/addon/lint/javascript-lint";
// for enabling code linting
import "codemirror/addon/lint/lint";
import "codemirror/addon/lint/lint.css";
import "codemirror/lib/codemirror.css";
// for javascript editor
import "codemirror/mode/javascript/javascript";
import CodeMirror from "codemirror";

const Editor: React.FC<SubplaygroundProps> = ({
  requestConfig,
  setRequestConfig,
}) => {
  const [body, setBody] = useState(JSON.stringify(requestConfig.body));

  useEffect(() => {
    setBody(JSON.stringify(requestConfig.body));
  }, [requestConfig.body]);

  const handleChange = (editor, data, value) => {
    try {
      let code = JSON.parse(value);
      setRequestConfig((prev) => ({
        ...prev,
        body: code,
      }));
    } catch (error) {
      console.log(error);
    }
    setBody(value);

    // const errors = CodeMirror.lint.javascript(value, data, editor);

    // if ((errors as any).length) {
    //   console.log(errors);
    //   return;
    // }

    // let code = JSON.parse(value);
    // setRequestConfig((prev) => ({
    //   ...prev,
    //   body: code,
    // }));
  };

  //console.log(body);

  return (
    <div>
      <CodeEditor
        onBeforeChange={handleChange}
        value={body}
        options={{
          lineNumbers: true,
          mode: "application/json",
          autoCloseBrackets: true,
          gutters: ["CodeMirror-lint-markers"],
          lint: true,
        }}
      />
    </div>
  );
};

export default Editor;
