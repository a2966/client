import { METHODS } from '.';

export interface DataType {
  project_name: string;
  base_url: string;
  api_details: {
    name: string;
    url: string;
    method: METHODS;
    body: Object;
    query_params: {
      key?: string;
      value?: string;
      description?: string;
    }[];
    headers: {
      key?: string;
      value?: string;
      description?: string;
    }[];
    id: number;
  }[];
}
export interface ContextDataType {
  data: Partial<DataType>;
  selectedIndex: number;
}
export interface ContextType {
  contextData: ContextDataType;
  setContextData: React.Dispatch<React.SetStateAction<ContextDataType>>;
}
export interface AlertContextType {
  isOpen: boolean;
  message: string;
  type: string;
}

export interface AlertType {
  alert: AlertContextType;
  setAlert: React.Dispatch<React.SetStateAction<AlertContextType>>;
}
