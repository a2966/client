export interface SearchBarTypes {
  value: string;
  setValue: React.Dispatch<React.SetStateAction<string>>;
}

export interface List {
  name: string;
  url: string;
  method: string;
  body: Object;
  query_params: {
    key?: string;
    value?: string;
    description?: string;
  }[];
  headers: {
    key?: string;
    value?: string;
    description?: string;
  }[];
  id: number;
}

export interface ApiListsProps {
  list: List[];
}
