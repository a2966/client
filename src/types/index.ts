export interface KeyValuePair {
  key?: string;
  value?: string;
  description?: string;
}

export type ApiMethodType = 'GET' | 'POST' | 'PUT' | 'DELETE';

export interface RequestConfig {
  headers: KeyValuePair[];
  query_params: KeyValuePair[];
  body: Object;
  method: ApiMethodType;
  url: string;
  name: string;
  id: number;
}

export type Columns = 'key' | 'value' | 'description';

export interface SubplaygroundProps {
  requestConfig: RequestConfig;
  setRequestConfig: React.Dispatch<React.SetStateAction<RequestConfig>>;
}
