export const columns = [
  { id: 1, name: "KEY" },
  { id: 2, name: "VALUE" },
  { id: 3, name: "DESCRIPTION" },
];

export const apiMethods = [
  {
    id: 1,
    value: "GET",
  },
  {
    id: 2,
    value: "POST",
  },
  {
    id: 2,
    value: "PUT",
  },
  {
    id: 4,
    value: "PATCH",
  },
  {
    id: 5,
    value: "DELETE",
  },
];

export const upperTabs = [
  { id: 1, name: "query_params", value: "Params" },
  { id: 2, name: "headers", value: "Headers" },
  { id: 3, name: "body", value: "Body" },
];
