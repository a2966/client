import { createContext } from "react";
import { AlertType, ContextType } from "../types/contextTypes";

export const MainContext = createContext<ContextType>({
  contextData: {
    data: {},
    selectedIndex: -1,
  },
  setContextData: () => {},
});

export const AlertContext = createContext<AlertType>({
  alert: {
    isOpen: false,
    message: '',
    type: ''
  },
  setAlert: () => {}
})