import { useState } from "react";
import Playground from "./components/mainplayground";
import SideTab from "./components/sidetab";
import { AlertContext, MainContext } from "./context";
import postData from "./data.json";
import { AlertContextType, ContextDataType, DataType } from "./types/contextTypes";

function App() {
  const [contextData, setContextData] = useState<ContextDataType>({
    data: postData as DataType,
    selectedIndex: -1,
  });

  const [alert, setAlert] = useState<AlertContextType>({
    isOpen: false,
    message: '',
    type: ''
  })

  return (
    <MainContext.Provider value={{ contextData, setContextData }}>
      <AlertContext.Provider value={{ alert, setAlert }}>
        <div className="h-screen w-screen flex">
          <div className="w-1/4 ">
            <SideTab />
          </div>
          <div className="w-3/4 ">
            <Playground />
          </div>
        </div>
      </AlertContext.Provider>
    </MainContext.Provider>
  );
}

export default App;
