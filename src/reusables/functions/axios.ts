import axios, { AxiosRequestConfig } from 'axios';
import { ApiMethodType } from '../../types';

const axios_config = (
  url: string,
  params: Object,
  headers: Object,
  method: ApiMethodType,
  data: Object
): AxiosRequestConfig => ({
  method,
  url,
  data,
  params,
  headers,
});

export const async_func_data = async (
  url: string,
  method: ApiMethodType,
  params: Object,
  headers: Object,
  data: Object
) => {
  try {
    const axiosConfig = axios_config(url, params, headers, method, data);
    const response = await axios(axiosConfig);
    return response;
  } catch (error) {
    throw error.response;
  }
};
