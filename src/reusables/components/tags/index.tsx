import React from "react";

const getClassName = (method: string) => {
  switch (method) {
    case "GET":
      return "bg-green-300 text-green-800 border-2 border-green-500";
    case "POST":
      return "bg-blue-300 text-blue-800 border-2 border-blue-500";
    case "PUT":
      return "bg-gray-300 text-gray-800 border-2 border-gray-500";
    case "DELETE":
      return "bg-red-300 text-red-800 border-2 border-red-500";
  }
};

const Tags: React.FC<{ method: string }> = ({ method }) => {
  return (
    <p
      className={`rounded-md w-13 h-5 p-1 outline-none font-semibold text-xs grid place-content-center ${getClassName(
        method
      )}`}
    >
      {method}
    </p>
  );
};

export default Tags;
