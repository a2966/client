import React, { useState } from "react";
import { SearchBarTypes } from "../../../types/searchBarTypes";
import { FaSearch } from "react-icons/fa";

export const UseSearchComponent = (initialValue: string) => {
  const [value, setValue] = useState<string | "">(initialValue);

  return {
    searchProps: {
      value,
      setValue,
    },
  };
};

const SearchBar: React.FC<SearchBarTypes> = ({ setValue, value }) => {
  return (
    <div className='w-full pl-4 pr-4'>
      <div className='relative w-full pl-4 pr-4 bg-gray-50 mb-12'>
        <input
          type='text'
          placeholder='Search...'
          className='h-12 w-full outline-none border-none focus:outline-none bg-transparent placeholder-gray-400'
          value={value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setValue(e.target.value);
          }}
        />
        <div className='absolute right-4 top-0 h-full grid place-content-center'>
          <FaSearch className='text-dark2' />
        </div>
      </div>
    </div>
  );
};

export default SearchBar;
