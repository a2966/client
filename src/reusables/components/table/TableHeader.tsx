import React from "react";
import { columns } from "../../../constants";

const TableHeader = () => {
  return (
    <div className="w-full flex border border-gray-200">
      {columns.map((column, i) => (
        <div
          className={`flex-1 text-xs font-medium text-dark2 px-3 py-2 ${
            i !== columns.length - 1 && "border-r border-gray-200"
          }`}
          key={column.id}
        >
          {column.name}
        </div>
      ))}
    </div>
  );
};

export default TableHeader;
