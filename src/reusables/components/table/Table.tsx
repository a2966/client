import React, { useContext, useState } from 'react';
import { IoMdAddCircle } from 'react-icons/io';
import { AlertContext } from '../../../context';
import useRenderCount from '../../../hooks/useRenderCount';
import { Columns, KeyValuePair, SubplaygroundProps } from '../../../types';
import TableHeader from './TableHeader';
import TableRow from './TableRow';

const Table: React.FC<
  SubplaygroundProps & { currentTab: number; currentKey: string }
> = ({ requestConfig, setRequestConfig, currentTab, currentKey }) => {
  const [focusedRow, setFocuesedRow] = useState<number | null>(null);

  useRenderCount('Table');

  const { setAlert } = useContext(AlertContext);

  const addRow = () => {
    let newRow: KeyValuePair = { key: '', value: '', description: '' };
    setRequestConfig((prev) => {
      return { ...prev, [currentKey]: [...prev[currentKey], newRow] };
    });

    setAlert({
      isOpen: true,
      message: 'New row added',
      type: 'info',
    });
  };

  const handleChange = (rowIndex: number, column: Columns, value: string) => {
    let newParams = requestConfig[currentKey];
    newParams[rowIndex][column] = value;
    setRequestConfig((prev) => {
      return { ...prev, [currentKey]: newParams };
    });
  };

  const handleDelete = (rowIndex: number) => {
    let newParams = requestConfig[currentKey].filter((_, i) => i !== rowIndex);
    setRequestConfig((prev) => {
      return { ...prev, [currentKey]: newParams };
    });

    setAlert({
      isOpen: true,
      message: 'Row Deleted',
      type: 'warning',
    });
  };

  console.log(requestConfig);

  return (
    <div className='h-full mt-2'>
      <p className='text-sm font-medium text-lightGray mb-2'>
        Query {currentTab === 1 ? 'Params' : 'Headers'}
      </p>
      <TableHeader />
      {requestConfig[currentKey].map((param, idx) => (
        <TableRow
          index={idx}
          key={idx}
          row={param}
          focusedRow={focusedRow}
          setFocuesedRow={setFocuesedRow}
          handleChange={handleChange}
          handleDelete={handleDelete}
          currentTab={currentTab}
        />
      ))}
      <button
        onClick={addRow}
        className='mt-2 opacity-50 hover:opacity-100 transition-all text-lightGray text-xs flex items-center'
      >
        <IoMdAddCircle size={16} className='mr-1' /> Add{' '}
        {currentTab === 1 ? 'Params' : 'Headers'}
      </button>
    </div>
  );
};

export default Table;
