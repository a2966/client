import React from 'react';
import { useState } from 'react';
import { IoCloseCircle } from 'react-icons/io5';

import { columns } from '../../../constants';
import { Columns, KeyValuePair } from '../../../types';
import TableItem from './TableItem';

interface Props {
  row: KeyValuePair;
  index: number;
  focusedRow: number | null;
  setFocuesedRow: React.Dispatch<React.SetStateAction<number | null>>;
  handleChange: (rowIndex: number, column: Columns, value: string) => void;
  handleDelete: (rowIndex: number) => void;
  currentTab: number;
}

const TableRow: React.FC<Props> = ({
  index,
  row,
  focusedRow,
  setFocuesedRow,
  handleChange,
  handleDelete,
  currentTab,
}) => {
  const [showDelete, setShowDelete] = useState(false);
  const [showText, setShowText] = useState(false);

  return (
    <div
      className={` relative w-full flex border border-gray-200 border-t-0 transition-all ${
        focusedRow === index && 'bg-gray-50'
      } ${showText && 'bg-red-50'}`}
      onMouseEnter={() => setShowDelete(true)}
      onMouseLeave={() => setShowDelete(false)}
    >
      {showDelete && (
        <>
          {showText && (
            <p className='absolute h-full  right-8 text-xs flex items-center text-red-600'>
              Delete {currentTab === 1 ? 'Params' : 'Headers'}
            </p>
          )}
          <button
            onMouseEnter={() => setShowText(true)}
            onMouseLeave={() => setShowText(false)}
            onClick={() => handleDelete(index)}
            className='absolute h-full right-2 opacity-50 hover:opacity-100 hover:text-red-600 transition-all text-lightGray text-xs flex items-center'
          >
            <IoCloseCircle size={18} />
          </button>
        </>
      )}
      {columns.map((column, i) => (
        <TableItem
          column={column}
          columnsLength={columns.length}
          focusedRow={focusedRow}
          handleChange={handleChange}
          i={i}
          index={index}
          row={row}
          setFocuesedRow={setFocuesedRow}
        />
      ))}
    </div>
  );
};

export default TableRow;
