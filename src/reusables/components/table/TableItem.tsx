import React from 'react';
import { Columns, KeyValuePair } from '../../../types';

interface Props {
  row: KeyValuePair;
  column: {
    id: number;
    name: string;
  };
  i: number;
  columnsLength: number;
  focusedRow: number | null;
  index: number;
  setFocuesedRow: React.Dispatch<React.SetStateAction<number | null>>;
  handleChange: (rowIndex: number, column: Columns, value: string) => void;
}

const TableItem: React.FC<Props> = ({
  column,
  i,
  columnsLength,
  focusedRow,
  handleChange,
  index,
  setFocuesedRow,
  row,
}) => {
  return (
    <div
      className={`flex-1 text-xs font-medium text-dark2 px-2 py-1 ${
        i !== columnsLength - 1 && 'border-r border-gray-200'
      }`}
      key={column.id}
    >
      <input
        className={`w-full outline-none p-1 border ${
          focusedRow === index ? 'border-gray-200' : 'border-transparent'
        }`}
        name={column.name}
        onChange={(e) =>
          handleChange(
            index,
            (column as any).name.toLowerCase(),
            e.target.value
          )
        }
        onFocus={() => {
          focusedRow !== index && setFocuesedRow(index);
        }}
        value={(row as any)[column.name.toLowerCase()]}
        placeholder={column.name.toLowerCase()}
      />
    </div>
  );
};

export default TableItem;
