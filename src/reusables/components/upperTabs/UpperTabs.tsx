import React, { useEffect } from 'react';
import { upperTabs } from '../../../constants';

interface Props {
  currentTab: number;
  setCurrentTab: React.Dispatch<React.SetStateAction<number>>;
}

const UpperTabs: React.FC<Props> = ({ currentTab, setCurrentTab }) => {
  return (
    <div className='w-full flex mt-2'>
      {upperTabs.map((tab) => (
        <button
          key={tab.name}
          className={`
            ${tab.id !== 1 && ' ml-10 '} hover:text-dark1 
            ${
              tab.id === currentTab
                ? ' text-dark1 border-b-2 border-primary '
                : 'text-lightGray'
            } 
            item-center font-medium text-md
          `}
          onClick={() => setCurrentTab(tab.id)}
        >
          {tab.value}
        </button>
      ))}
    </div>
  );
};

export default UpperTabs;
