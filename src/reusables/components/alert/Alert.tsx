import { useContext, useEffect, useState } from 'react'
import { AlertContext } from '../../../context'

const response = (type: string) => {
    switch(type){
        case 'success':
            return {
                classes: `bg-green-100 border-l-4 border-green-400 text-green-700 p-4`,
                text: 'Success'
            }
        case 'warning':
            return {
                classes: `bg-yellow-100 border-l-4 border-yellow-500 text-yellow-700 p-4`,
                text: 'Warning'
            }
        case 'info':
            return {
                classes: `bg-blue-100 border-l-4 border-blue-500 text-blue-700 p-4`,
                text: 'Info'
            }
        case 'error':
        default: 
            return {
                classes: `bg-red-100 border-l-4 border-red-500 text-red-700 p-4`,
                text: 'Error'
            }
    }
}

const Alert = () => {
    const { alert, setAlert } = useContext(AlertContext)

    const [store, setStore] = useState(response(""))

    useEffect(() => {
        setStore(response(alert.type))
    }, [alert])

    useEffect(() => {
        if(alert.isOpen){
            setTimeout(() => {
                setAlert({
                    isOpen: false,
                    message: '',
                    type: ''
                })
            }, 4000)
        }
    }, [alert.isOpen])

    return (
        <div className={`
            w-2/4 border-2 transition-all absolute z-10 left-1/4 ${store.classes}
            ${alert.isOpen ? 'bottom-10' : '-bottom-20'}
        `} role="alert">
            <p className="font-bold">{ store.text }</p>
            <p>{ alert.message }</p>
        </div>
    )
}

export default Alert
