import { useEffect, useRef } from "react";

/**
 *
 * @param cb Callback function to be debounced
 *
 * @param delay time for delay
 *
 * @param val value which is being changed
 */
export const useDebounce = (cb: any, delay: number = 500, val: any) => {
  if (typeof delay !== "number") {
    throw new Error("Delay is not a number");
  }

  if (typeof cb !== "function") {
    throw new Error("fn is not a function");
  }
  const refForForceStop = useRef(false);

  useEffect(() => {
    let timeOut: any;
    if (!refForForceStop.current) {
      refForForceStop.current = true;
    } else {
      timeOut = setTimeout(() => {
        cb(val);
      }, delay);
    }
    return () => {
      clearTimeout(timeOut);
    };
  }, [delay, val]);
};
