module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#0AA1E2',
        dark1: '#1E1E2F',
        dark2: '#27293D',
        light1: '#F5F6FA',
        light2: '#FFFFFF',
        lightGray: '#6b6b6b',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
